# Gyoza Drupal Profile
_A Drupal 7.x Install Profile_

### Installation
Using Drush, install the Drupal instance from the commandline.

`drush site-install gyoza_profile --db-url=mysql://root:pass@localhost:port/dbname`

Installs the following themes:

- bootstrap (Base)
- adminimal_theme (Admin)

Installs the following modules:

- admin_menu
- adminimal_admin_menu
- backup_migrate
- context
- ctools
- devel
- ds (Display Suite)
- ds_bootstrap_layouts (Display Suite Layouts w/ Bootstrap)
- jquery_update
- libraries
- pathauto
- token
- views
- ckeditor
- link
- module_filter

Installs the following libraries:

- ckeditor 3.6.2
