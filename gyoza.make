api = 2
core = 7.x
projects[] = drupal

::
;; Modules
;;
defaults[projects][subdir] = contrib
projects[] = admin_menu
projects[] = adminimal_admin_menu
projects[] = backup_migrate
projects[] = context
projects[] = ctools
projects[] = devel
projects[] = ds
projects[] = ds_bootstrap_layouts
projects[] = jquery_update
projects[] = libraries
projects[] = pathauto
projects[] = token
projects[] = views
projects[] = ckeditor
projects[] = link
projects[] = module_filter
;projects[] = date
;projects[] = mollom
;projects[] = field_group
;projects[] = google_analytics
;projects[] = views_bulk_operations

;;
;; Base Themes
;;
projects[] = bootstrap
projects[] = adminimal_theme

;;
;; Libraries
;;
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][destination] = libraries
libraries[ckeditor][directory_name] = ckeditor
